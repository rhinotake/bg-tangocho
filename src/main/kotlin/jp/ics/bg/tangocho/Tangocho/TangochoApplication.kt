package jp.ics.bg.tangocho.Tangocho

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class TangochoApplication

fun main(args: Array<String>) {
    SpringApplication.run(TangochoApplication::class.java, *args)
}
